You will need a few packages to build **pyftdc**.  
Also, Python3 and Python 3 developent libraries


For Ubuntu:
``` 
sudo apt install build-essential
sudo apt install -y python3 python3-dev python3-pip
sudo apt install -y cmake  
sudo apt install -y zlib1g-dev
```

For Almalinux 
```
sudo dnf install make automake gcc gcc-c++ kernel-devel
sudo yum install -y python3 python3-devel
sudo yum install -y cmake
sudo yum install -y zlib1g-dev z-devel
```

 