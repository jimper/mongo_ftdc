//
// Created by jorge on 3/13/22.
//

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN 1 // in only one src file
#include <boost/test/unit_test.hpp>
#include <FTDCParser.h>
#include <Chunk.h>
#include <iostream>
#include <filesystem>
#include <utility>


static const char *DATA_TEST_DIR = "../../Boost_tests/version_data/";
static std::vector<std::string> testFiles;

std::vector<std::string>
getAllFiles() {
    namespace fs = std::filesystem;
    using namespace std;

    if (testFiles.empty()) {
        auto path = fs::absolute(DATA_TEST_DIR);
        try {
            for (auto & fileInPath: fs::directory_iterator(path)) {
                if (!fileInPath.is_directory()) {  //  && fileInPath.path().extension() == "ftdc")
                    testFiles.emplace_back(fileInPath.path().string());
                }
            }
        }
        catch (fs::filesystem_error) {
            cerr << "Error while iterating directory '" << path.string() << "' for tests" << endl;
        }
    }
    return testFiles;
}

std::string
getFileWithVersionAndSuffix(std::string version, std::string suffix) {
    namespace fs = std::filesystem;
    using namespace std;

    auto allFiles = getAllFiles();
    for (auto &f : allFiles) {
        auto name = fs::path(f).filename().string();
        auto ending = fs::path(f).extension().string();
        if (fs::path(f).filename().string().find(version) != string::npos
            && fs::path(f).extension().string().find(suffix) != string::npos)  {
            return f;
        }
    }
    return "";
}

std::string
getMetricFileWithVersion(std::string version) {
    return getFileWithVersionAndSuffix(std::move(version), "ftdc");
}

std::string
getCSVFileWithVersion(std::string version) {
    return getFileWithVersionAndSuffix(std::move(version), "csv");
}

int
testTimestamps(const std::string& infix) {
    auto ftdcFileName = getMetricFileWithVersion(infix);
    auto csvFileName = getCSVFileWithVersion(infix);

    if (!ftdcFileName.empty() && !csvFileName.empty()) {

        // Construct a dataset from a CSV file
        auto csvDataset = new Dataset(csvFileName);
        auto metricNamesCSV = csvDataset->getMetricsNames();

        // Parse a diagnostic file and get the dataset.
        auto parser = new FTDCParser();
        parser->setVerbose(true);
        auto dataset = parser->parseFile(ftdcFileName);
        auto metricNamesParsed = dataset->getMetricsNames();

        // Timestamps
        auto tsParsed = dataset->getMetric("start");
        auto tsCSV = csvDataset->getMetric("start");
        BOOST_CHECK_GT(tsParsed->size(), tsCSV->size());

        for (size_t i = 0; i < tsCSV->size(); ++i) {
            if (tsParsed->at(i) != tsCSV->at(i)) {
                BOOST_TEST_FRAMEWORK_MESSAGE("At position ");
                BOOST_CHECK_EQUAL(tsParsed->at(i), tsParsed->size());
            }
        }
        return 0;
    }
    else {
        return 1;
    }
}


int
dumpMetricNames(const std::string & infix) {
    using namespace std;

    auto ftdcFileName = getMetricFileWithVersion(infix);

    if (!ftdcFileName.empty()) {
        // Parse a diagnostic file and get the dataset.
        auto parser = new FTDCParser();
        parser->setVerbose(true);
        auto dataset = parser->parseFile(ftdcFileName);

        // check metrics names counts
        auto metricNamesParsed = dataset->getMetricsNames();
        BOOST_TEST_MESSAGE("------------------------------------ metric names ------------------------------------");
        for (auto &mn : metricNamesParsed)
            BOOST_TEST_MESSAGE(mn.c_str());


        BOOST_CHECK_EQUAL(1,1);

        return 0;
    }
    else {
        return 1;
    }
}

int
testMetricNames(const std::string& infix) {
    using namespace std;

    auto ftdcFileName = getMetricFileWithVersion(infix);
    auto csvFileName = getCSVFileWithVersion(infix);

    if (!ftdcFileName.empty() && !csvFileName.empty()) {

        // Construct a dataset from a CSV file
        auto csvDataset = new Dataset(csvFileName);
        auto metricNamesCSV = csvDataset->getMetricsNames();

        // Parse a diagnostic file and get the dataset.
        auto parser = new FTDCParser();
        parser->setVerbose(true);
        auto dataset = parser->parseFile(ftdcFileName);

        // check metrics names counts
        auto metricNamesParsed = dataset->getMetricsNames();

        BOOST_CHECK_EQUAL(metricNamesCSV.size(), metricNamesParsed.size());

        vector<string> notFound;
        for (size_t i=0; i<metricNamesParsed.size(); ++i) {
            bool found = false;
            string metric;
            for (size_t j=0; j<metricNamesCSV.size(); ++j) {

                if (metricNamesParsed[i] == metricNamesCSV[j]) {
                    found = true;
                    break;
                }
            }
            if (!found)
                notFound.emplace_back(metricNamesParsed[i]);
        }

        BOOST_CHECK_EQUAL(0,notFound.size());
        if (notFound.size()>0) {
            BOOST_TEST_MESSAGE("------------------------------------ metric names not found ------------------------------------");
            for (auto &nf: notFound) {
                BOOST_TEST_MESSAGE(nf.c_str());
            }
        }

        return 0;
    }
    else {
        return 1;
    }
}


BOOST_AUTO_TEST_CASE(metric_names_4_0) {
    dumpMetricNames("4.0");
    auto test = testMetricNames("4.0");
    BOOST_CHECK_EQUAL(test,0);
}

BOOST_AUTO_TEST_CASE(timestamps_4_0) {
    auto test = testTimestamps("4.0");
    BOOST_CHECK_EQUAL(test,0);
}

BOOST_AUTO_TEST_CASE(metric_names_4_2) {
    auto test = testMetricNames("4.2");
    BOOST_CHECK_EQUAL(test,0);
}

BOOST_AUTO_TEST_CASE(timestamps_4_2) {

    auto test = testTimestamps("4.2");
    BOOST_CHECK_EQUAL(test,0);
}

#if 0
BOOST_AUTO_TEST_CASE(timestamps_4_4) {

    auto test = testTimestamps("4.4");
    BOOST_CHECK_EQUAL(test,0);
}


BOOST_AUTO_TEST_CASE(timestamps_5_0) {

    auto test = testTimestamps("5.0");
    BOOST_CHECK_EQUAL(test,0);
}

#endif

BOOST_AUTO_TEST_CASE(metric_names_6_0) {
    auto test = testMetricNames("6.0");
    BOOST_CHECK_EQUAL(test,0);
}

BOOST_AUTO_TEST_CASE(timestamps_6_0) {

    auto test = testTimestamps("6.0");
    BOOST_CHECK_EQUAL(test,0);
}
