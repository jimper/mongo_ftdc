import pickle
from time import time

from pymongo import MongoClient
from threading import Thread, Event
from time import sleep


class PeriodicTimer(Thread):
    """A periodic timer that runs indefinitely until cancel() is called."""

    def __init__(self, interval, function, args=None, kwargs=None):
        Thread.__init__(self)
        self.interval = interval
        self.function = function
        self.args = args if args is not None else []
        self.kwargs = kwargs if kwargs is not None else {}
        self.finished = Event()

    def cancel(self):
        """Stop the timer if it hasn't finished yet."""
        self.finished.set()

    def run(self):
        """Run until canceled"""
        while not self.finished.wait(self.interval):
            self.function(*self.args, **self.kwargs)


def get_client(uri, usr, pwd):
    if usr and pwd:
        return MongoClient(uri,
                           username=usr,
                           pwd=pwd)
    else:
        return MongoClient(uri)


data_list=[]


def get_ftdc(admin):
    start = time()
    frame = admin.command({'getDiagnosticData': 1})

    # There are two elements in the dictionary: data and ok.
    data_list.append(frame['data'])

    print(f'{start} : {time()} : {time() - start}')


def capture_ftdc(mongo_uri, mongo_user, mongo_pwd, span, file_name):
    client = get_client(uri=mongo_uri, usr=mongo_user, pwd=mongo_pwd)
    db = client.get_database('admin')
    sleep(3)
    if client:
        rt = PeriodicTimer(1, get_ftdc, args={db})
        rt.start()
        sleep(span)
        rt.join(1)
        rt.cancel()
        pickle.dump(data_list, file=open(file_name, 'wb'))


def load_pickled_ftdc(file_name):
    with open(file_name, 'rb') as f:
        frame = pickle.load(f)
        return frame

# These should go in as command line args
URI = 'mongodb://whale.local:27017'
USER = None
PWD = None
OUTPUT_FILE = 'ftdc.pickle'

if __name__ == '__main__':

    print("capturing...")
    capture_ftdc(URI, USER, PWD, 60, OUTPUT_FILE)

    f = load_pickled_ftdc(OUTPUT_FILE)
    print(f'len = {len(f)}')
