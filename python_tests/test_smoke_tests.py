import json
import unittest
from time import sleep

import pyftdc

class MyTestCase(unittest.TestCase):
    root_test_files = '../Boost_tests/version_data/'
    diagnostic_data = '../Boost_tests/diagnostic.data/'

    def test_no_file(self):
        file_path = self.root_test_files + 'dfsdfsdfsdfsdfsdf'
        parser = pyftdc.FTDCParser()
        parser.set_verbose(False)
        dataset = parser.parse_file(file_path)

        if dataset:
            ts = dataset.get_metrics('start')
            ts_size = len(ts)
            print(f'Timestamp count {ts_size}. Start:{ts[0]}  Last: {ts[-1]}')
            print(f'Metadata: {parser.metadata}')
            print(f'Metrics: {parser.testMetricNames}')

    def test_single_file(self):
        file_path = self.root_test_files
        parser = pyftdc.FTDCParser()
        parser.set_verbose(False)
        dataset = parser.parse_file(file_path)
        if dataset:
            ts = dataset.get_metrics('start')
            ts_size = len(ts)
            print(f'Timestamp count {ts_size}. Start:{ts[0]}  Last: {ts[-1]}')
            print(f'Metadata: {parser.metadata}')
            print(f'Metrics: {parser.testMetricNames}')

    def test_single_file_numpy(self):
        file_path = self.diagnostic_data + 'metrics.2023-02-15T19-44-25Z-00000'
        parser = pyftdc.FTDCParser()
        parser.set_verbose(False)
        dataset = parser.parse_file(file_path)
        if dataset:
            ts = dataset.get_metrics_numpy('start')
            ts_size = len(ts)
            print(f'Timestamp count {ts_size}. Start:{ts[0]}  Last: {ts[-1]}')
            print(f'Metadata: {parser.metadata}')
            print(f'Metrics: {parser.testMetricNames}')

            command_names = ['start', 'end']
            commands = dataset.get_metrics_list_numpy(command_names)

            # cases when one or more do not exist.
            nope = dataset.get_metrics_numpy('nope no no')

            # Non-existing metrics in a list
            no = dataset.get_metrics_list_numpy(['start', 'i dont belong here', 'end'])




    """ 
    def test_single_4_0(self):
        self.single_file('metrics.4.0.23.ftdc')

    def test_single_4_2(self):
        self.single_file('metrics.4.2.20.ftdc')

    def test_single_4_4(self):
        self.single_file('metrics.4.4.11.ftdc')

    def test_single_5_0(self):
        self.single_file('metrics.5.0.11.ftdc')

    def test_single_6_0(self):
        self.single_file('metrics.6.0.1.ftdc')
    """

    def test_dir(self):
        multi_parser = pyftdc.FTDCParser()
        datasets = multi_parser.parse_dir(self.diagnostic_data)
        ds_count = len(datasets)
        print(f'There are {ds_count} datasets from {self.diagnostic_data}')
        if ds_count > 0:
            for ds in datasets:
                if ds:
                    meta_dict = json.loads(ds.metadata)
                    print(f'File: {ds.file}')
                    # print(f'{meta_dict}')

                    if not ds.interim:
                        ts = ds.get_metrics("start")
                        ts_numpy = ds.get_metrics_numpy('start')
                        if ts:
                            ts_size = len(ts)
                            print(f'Timestamp count {ts_size}. Start:{ts[0]}  Last: {ts[-1]}')

                        else:
                            print(f'No timestamps on this dataset.')
                else:
                    print(f'Bad dataset from file {ds}')


#
# 'metrics.4.4.11.ftdc'
# 'metrics.5.0.11.ftdc'
# 'metrics.6.0.1.ftdc'

if __name__ == '__main__':
    # print(f'Starting tests in 30 seconds')
    # sleep(15)
    # print(f'Starting tests in 15 seconds')
    # sleep(15)
    print(f'Starting tests now')

    unittest.main()
